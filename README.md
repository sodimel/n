<div align="center"><img src="logo.png" alt="n.py logo" /></div>

# n.py − very simple note taking cli tool

## Install

* Download the script & make it executable:
    ```bash
    wget https://git.bitmycode.com/sodimel/n/-/raw/master/n.py && chmod +x n.py
    ```
* Move it somewhere, like in ~/.local/
    ```bash
    mv n.py ~/.local
    ```
* Create a simlink in a folder in your path:
    ```bash
    sudo ln -s ~/.local/n.py /usr/bin/n
    ```
* Handle storage of notes:
    * Create a folder named `n` (default):
        ```bash
        mkdir ~/n
        ```
    * **OR** create a special folder and add `N_FOLDER` env var in your `.(ba|zs|...)hrc`:
        ```bash
        mkdir ~/notes
        echo "export N_FOLDER='~/notes' >> ~/.your_shrc_file"
        ```
* *Autocompletion (optional): See output of `n autocomplete` command*
* ![That's all folks!](https://git.bitmycode.com/sodimel/n/uploads/e0627cdeb4089b384e205b1c6842377a/image.png)

## Help

```
Usage:
  ./n.py [OPTION] [str]

Args:
  h, help, -h, --help       Display this (very long) help text in your shell/terminal.
  n, new, -n, --new         Create a new file. Default name is today : '%Y-%m-%d.txt'.
                            Add any str to create a named new file, e.g. './n.py c hi'
                            will create a file in 'N_FOLDER' or ~/n/, with the name of
                            'hi.txt' using 'N_TOOL' or 'nano'. The command will open a
                            file if it already exist, effectively allowing you to edit
                            it. Calling the 'n.py' script with no argument will launch
                            this exact same function. And entering an unrecognised arg
                            will create a new file named after the aforementioned arg.
                            Add an other argument to define the extension of the file.
  s, search, -s, --search   Search a string in all files in 'N_FOLDER' or '~/n/', this
                            command sort the result with the latest opened file first.
  c, cat, -c, --cat         Print a file (if found) on plain ol' stdout. Useful if you
                            want to do some special grep after: just pipe the content!
                            Can print content of multiple files, sorted by most recent
                            updated first. Good if you want to cat your private diary.
                            You can use str substitution if you enter no arg, and then
                            just enter words or commands or an asterisk in the prompt.
  l, ls, -l, --ls           List files in 'N_FOLDER', last modified first, in one col.
                            Add argument to list only files where name is in '*name*'.

Env vars:
  N_FOLDER                  This is the folder name where the script will save all the
                            files into. Will default to '~/n/' if the env var does not
                            exist. Please notice the trailing slash. You are in charge
                            of creating the folder if it does not exist: run (maybe) a
                            'mkdir ~/n' command before you run the n.py script a first
                            time, or else you will be experiencing an error, like this
                            one: [ Directory '/home/me/aaaa' does not exist ] (this is
                            the default result of using nano (which is the tool that's
                            used if no N_TOOL environment variable is found when using
                            n.py. Here is the exact command I ran in order to get that
                            previous output on my computer: 'N_FOLDER=~/aaaa/ ./n.py'.
                            A cool thing to do is to set it to something starting with
                            '$HOME/', like this: 'export N_FOLDER="$HOME/notes/"'. Its
                            really important to do this, in order to make autocomplete
                            like the one that's shared in this help work w/ any error.
  N_TOOL                    The tool used when creating/editing a note. The default is
                            nano, because it's simple to use and because a way to exit
                            is actually displayed on the bottom panel (yes, I refer to
                            this strange tool called vim). You may use this var in the
                            case of your OS don't have nano, or maybe if you just want
                            to use another tool (maybe vim, emacs, geany, gedit, code,
                            subl or any other program that can edit simple txt files).
  N_DEFAULT_EXT             Extension to save files if none is provided. Default value
                            is 'txt', but personally I set it to 'md' because nano has
                            a very cool syntax highlightning for markdown all content.
  N_DEFAULT_SHELL           Default to 'sh'. N.py is launching an interactive shell: I
                            wanted to be able to see md content with color output, and
                            so I switched from default 'cat' command to 'bat' (that is
                            awesome). I needed to be able to make Python run a command
                            with the .zshrc (for my case) loaded, and so I updated the
                            script, and then created this brand new env var to let you
                            do whatever you want. This feature is now added in v0.0.3.

Autocomplete:
                            I'm not really into all this bash-based stuff, but you can
                            find a small script and some instruction by executing this
                            command: 'n autocomplete', or 'python3 n.py autocomplete'.

Why another tool?
                            I used a certain number of note taking programs and others
                            text editing apps and concepts. Here's a small list of the
                            tools & concepts: Joplin, Notable, a strange concept named
                            Johnny•Decimal, a (secret) gitlab wiki, some post-it tools
                            like the xubuntu post-it app named Xpad, and an other tool
                            named StickyNotes. But I didnt stay under any of the apps,
                            because they didn't had all the features that I wanted. So
                            I decided to create this tool after discovering this great
                            post (https://thesephist.com/posts/inc/) and discovering a
                            note taking tool named "inc" (stands for "incremental"). I
                            really liked the philosphy behind this tool, and I created
                            n.py to be very similar to inc. But n.py still differs for
                            these few reasons (this is indeed not an exhaustive list):
                            - the command line interface is different: you cant launch
                            a REPL, except when running './n.py s'(earch) without arg.
                            - the name of a note is not a number (like in inc), but is
                            the date of today (in the form '%Y-%m-%d.txt'). So it's (I
                            think) easier to retrieve content relative to a date, e.g.
                            "ah yes I wrote this thought yesterday! Lets search in the
                            file named '(%Y-1)-%m-%d.txt', and see if I can find it!".
                            - this tool is waayyy smaller than inc, and packages a lot
                            less features than this other tool (for an example, it did
                            not comes with a REPL, an history of commands, or a single
                            file database. Don't expect it to replace it anytime soon!

Links of other tools/concepts:
                           Joplin ----------------------------- https://joplinapp.org/
                           Notable ------------------------------ https://notable.app/
                           Johnny•Decimal ----------------- https://johnnydecimal.com/
                           Gitlab Wiki - https://docs.gitlab.com/ee/user/project/wiki/
                           Xpad --------------------------- https://launchpad.net/xpad
                           StickyNotes -------------------------- https://git.io/JwHnC
                           inc --------------------- https://github.com/thesephist/inc

Examples:
  Create file               $ python3 n.py
                            'N_TOOL' 'N_FOLDER'2021/10/08.txt

  Create named file         $ ./n.py n todo
                            'N_TOOL' 'N_FOLDER'todo.txt

  Create named file         $ n done
                            'N_TOOL' 'N_FOLDER'done.txt

  List all files            $ n l
                            cd 'N_FOLDER'; if [ $(ls -A 2>/dev/null | wc -l) -ne 0 ]; \
                            then ls --color -1t 'N_FOLDER'; else exit 1; fi
                            done.txt
                            todo.txt
                            2021/10/08.txt

  Cat all files which       $ ./n.py cat todo
  is in *todo*              n cat todo
                            cd 'N_FOLDER'; if [ $(ls -A *todo* 2>/dev/null | wc -l) -ne 0 ]; \
                            then cat $(ls *todo*); else exit 1; fi
                            TODO
                            - create cat command
                            - create ls command

  Cat files (prompt         $ n c
  with a *), error          What do you want to cat?
                            > *de
                            cd 'N_FOLDER'; if [ $(ls -A **de* 2>/dev/null | wc -l) -ne 0 ]; \
                            then cat $(ls **de*); else exit 1; fi
                            No *de file found in 'N_FOLDER'.

  Cat files (prompt         $ n c
  with a *), success        What do you want to cat?
                            > *do
                            cd 'N_FOLDER'; if [ $(ls -A **do* 2>/dev/null | wc -l) -ne 0 ]; \
                            then cat $(ls **do*); else exit 1; fi
                            DONE
                            - created a cat command, lets see if it works
                            TODO
                            - create cat command
                            - create ls command

  Search for "create"       $ python3 n.py s create
  string in all files       cd 'N_FOLDER'; grep --color=ALWAYS "todo" $(ls -1t); exit $?;
                            todo.txt:- create cat command
                            todo.txt:- create ls command
```

----

## Screenshot

![screenshot](https://git.bitmycode.com/sodimel/n/uploads/182e7d739672ad2aee34e8180c8d03ad/image.png)

----

## Misc

* if your OS only have Python2 (seriously?), replace `#!/usr/bin/python3` by `#!/usr/bin/python2`
* the `help section` of `n.py` was written using my poor english (bonjour baguette I'm french) after reading [this thread](https://twitter.com/mattgemmell/status/1441497498573545478) on Twitter
* [Example of output when using `bat` instead of cat](https://git.bitmycode.com/sodimel/n/uploads/1c24731cdc5b661a09798d4ff78d3d74/image.png).

## Misc-but-this-time-its-related-to-n.py-sourcecode

* the command that will be launched will be displayed before n.py will run it, so if it fails, you should see why pretty fast
* if there's only one note, `grep` will not display filenames (but that's not a problem since there's only one note)
* `n.py` should run on any python version since version [2.4](https://www.python.org/download/releases/2.4/highlights/#new-or-upgraded-modules-and-packages), it only requires core `os`, `sys`, `subprocess`, and `datetime`
    * *edit: it **does** run on python2.4.0! [Here's a proof](https://git.bitmycode.com/sodimel/n/uploads/60958bfcd1c11d69fb1b535437144d1a/image.png)! (I used [pyenv](https://github.com/pyenv/pyenv/) to install python2.4.0 (inside a [vm](https://git.bitmycode.com/sodimel/n/uploads/00997c3821bf92c3e78484ff8c0ef628/image.png)))*

----

## Changelog

- ### `0.0.5` *[10/09/24]*
    - fix search command (`n s word`) to work even if there's spaces in filenames
- ### `0.0.4` *[28/06/22]*
    - allow creating a new note containing spaces (`n "a b c"` created 3 files, now `n "a b c"` create a file named `a b c`)
- ### `0.0.3` *[22/10/21]*
    - added `N_DEFAULT_SHELL` env var (default is `sh`).
    - subprocess is now used like this:
        ```python
        subprocess.call([default_shell, '-i', '-c', command])
        ```
    - created this changelog
- ### `0.0.2` *[13/10/21]*
    - cat files will now cat most recent first
- ### `0.0.1` *[08/10/21]*
    - added cat & ls features
    - added default extension env var
    - added ability to define ext of a new file
    - updated help txt
    - added examples to help txt
    - removed ugly exit_code var in bash commands
    - added autocomplete script
- ### `no version` *[07/10/21]*
    - published n.py!
